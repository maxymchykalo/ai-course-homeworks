import nltk
nltk.download('punkt')
from nltk.tokenize import word_tokenize
from nltk.tree import Tree
import sys

TERMINALS = """
Adj -> "country" | "dreadful" | "enigmatical" | "little" | "moist" | "red"
Adv -> "down" | "here" | "never"
Conj -> "and" | "until"
Det -> "a" | "an" | "his" | "my" | "the"
N -> "armchair" | "companion" | "day" | "door" | "hand" | "he" | "himself"
N -> "holmes" | "home" | "i" | "mess" | "paint" | "palm" | "pipe" | "she"
N -> "smile" | "thursday" | "walk" | "we" | "word"
P -> "at" | "before" | "in" | "of" | "on" | "to"
V -> "arrived" | "came" | "chuckled" | "had" | "lit" | "said" | "sat"
V -> "smiled" | "tell" | "were"
"""

NONTERMINALS = """
S -> NP VP | S Conj S | S Conj VP
NP -> N | Det N | Det AP N | Pronoun
AP -> Adj | AP Adj
VP -> V | V NP | V PP | Adv VP | V Adv | V NP PP
PP -> P NP | P Pronoun
Pronoun -> "himself" | "herself" | "itself" | "themselves"
"""

grammar = nltk.CFG.fromstring(NONTERMINALS + TERMINALS)
parser = nltk.ChartParser(grammar)


def main():

    # If filename specified, read sentence from file
    if len(sys.argv) == 2:
        with open(sys.argv[1]) as f:
            s = f.read()

    # Otherwise, get sentence as input
    else:
        s = input("Sentence: ")

    # Convert input into list of words
    s = preprocess(s)

    # Attempt to parse sentence
    try:
        trees = list(parser.parse(s))
    except ValueError as e:
        print(e)
        return
    if not trees:
        print("Could not parse sentence.")
        return

    # Print each tree with noun phrase chunks
    for tree in trees:
        tree.pretty_print()

        print("Noun Phrase Chunks")
        for np in np_chunk(tree):
            print(" ".join(np.flatten()))


def preprocess(sentence):
    """
    Convert `sentence` to a list of its words.
    Pre-process sentence by converting all characters to lowercase
    and removing any word that does not contain at least one alphabetic
    character.
    """
    # Tokenize the sentence
    tokens = word_tokenize(sentence)
    
    # Convert tokens to lowercase and filter out non-alphabetic words
    words = [token.lower() for token in tokens if any(c.isalpha() for c in token)]
    
    return words


def np_chunk(tree):
    """
    Return a list of all noun phrase chunks in the sentence tree.
    A noun phrase chunk is defined as any subtree of the sentence
    whose label is "NP" that does not itself contain any other
    noun phrases as subtrees.
    """
    np_chunks = []
    
    # Function to recursively search for NP chunks
    def find_np_chunks(subtree):
        # Check if the current subtree is an NP
        if subtree.label() == "NP":
            # Check if this NP contains any NP subtrees
            contains_np = any(child.label() == "NP" for child in subtree if isinstance(child, Tree))
            # If it does not contain other NPs, add it to the list
            if not contains_np:
                np_chunks.append(subtree)
            else:
                # Otherwise, continue searching within this NP for NP chunks
                for child in subtree:
                    if isinstance(child, Tree):
                        find_np_chunks(child)
        else:
            # If the current subtree is not an NP, search its children
            for child in subtree:
                if isinstance(child, Tree):
                    find_np_chunks(child)
    
    # Start the recursive search from the root of the tree
    find_np_chunks(tree)
    
    return np_chunks


if __name__ == "__main__":
    main()
